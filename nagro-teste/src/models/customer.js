const customers = [
  {
    id: 1,
    nome_empresa: "Blumenal",
    numero_cnpj: "17.390.913/0001-64",
    nome_produto: "Frete rápido",
    nome_servico: "Transporte",
  },
  {
    id: 2,
    nome_empresa: "John & Johon",
    numero_cnpj: "64.710.373/0001-39",
    nome_produto: "Sabão ultimate",
    nome_servico: "Material de limpeza",
  },
  {
    id: 3,
    nome_empresa: "Tina",
    numero_cnpj: "98.840.293/0001-10",
    nome_produto: "CD",
    nome_servico: "Música",
  },
  {
    id: 4,
    nome_empresa: "Clarence",
    numero_cnpj: "90.137.593/0001-25",
    nome_produto: "Água",
    nome_servico: "Ensencial",
  },
  {
    id: 5,
    nome_empresa: "Kamel",
    numero_cnpj: "22.321.125/0001-56",
    nome_produto: "Soja",
    nome_servico: "sementes",
  },
];

export default customers;
